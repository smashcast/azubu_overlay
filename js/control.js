(function (angular) {
    'use strict';

    var app = angular.module('overlay-app', ['ngResource', 'ngAnimate']),
	url = 'http://socket.qa3.azubu.org/node/overlay/',
	purl = 'http://socket.qa3.azubu.org/node/public/overlay/';

    app.factory('Settings', [
	'$resource',
	function ($resource) {
	    var u = purl + ':username',
		defaults = {
		    enabled: true,
		    animation: 'slide-down',
		    volume: 5,
		    image: 'img/default.png',
		    sound: 'snd/default.wav',
		    message: {
			text: '',
			animation: 'wiggly'
		    },
		    font: {
			face: 'Exo 2',
			size: 30,
			weight: 500,
			color: '#ffffff'
		    },
		    duration: 5
		};

	    function extend (dst) {
		var i = arguments.length - 1,
		    o = JSON.parse(JSON.stringify(dst)),
		    c = {};

		function copy (t, s) {
		    var v;

		    for (v in s) {
			if (typeof(s[v]) === 'object') {
			    if (!t.hasOwnProperty(v)) {
				t[v] = {};
			    }
			    copy(t[v], s[v]);
			} else if (!t.hasOwnProperty(v)) {
			    t[v] = s[v];
			}
		    }
		}

		for (; i >= 1; --i) {
		    copy(c, arguments[i]);
		}
		copy(o, c);

		return o;
	    }

	    function xf (r) {
		var d = r.data && typeof(r.data) === 'object' ? r.data : {},
		    f = d.follower || {},
		    s = d.subscriber || {},
		    df = { message: { text: '[user] is following.' } },
		    ds = { message: { text: '[user] has subscribed.' } };

		d.follow = extend(f, defaults, df);
		d.subscribe = extend(s, defaults, ds);

		return d;
	    }

	    return $resource(u, null, {
		get: {
		    interceptor: {
			response: xf,
			responseError: xf
		    }
		}
	    });
	}
    ]);

    app.factory('AudioPlayer', [
	'$document',
	function ($document) {
	    return {
		elem: $document[0].createElement('audio'),
		play: function (file, volume) {
		    this.elem.src = file;
		    this.elem.volume = volume || 1.0;
		    this.elem.play();
		}
	    };
	}
    ]);

    app.directive('animDone', [
	function () {
	    return {
		scope: { 'animDone': '&' },
		link: function (scope, elm) {
		    elm.bind('animationend', scope.animDone || function () {});
		}
	    };
	}
    ]);

    app.controller('overlay-controller', [
	'$scope', '$location', 'Settings', 'AudioPlayer',
	function ($scope, $location, Settings, AudioPlayer) {
	    var u = $location.path(), socket;

	    $scope.alert = false;

	    function onAlert (msg) {
		var all = $scope.allSettings, alert;

		if (!all[msg.type]) {
		    return;
		}

		alert = JSON.parse(JSON.stringify(all[msg.type]));

		$scope.fontStyle = {
		    color: alert.font.color,
		    'font-size': alert.font.size + 'px',
		    'font-family': '\'' + alert.font.face + '\'',
		    'font-weight': alert.font.weight
		};
		$scope.animStyle = {
		    'animation-name': alert.animation,
		    'animation-duration': alert.duration + 's',
		    'animation-timing-function': 'ease-out'
		};
		alert.message.text
		    = all[msg.type].message.text.replace('[user]', msg.username);
		$scope.settings = alert;

		if ($scope.settings.enabled) {
		    $scope.alert = true;
		    AudioPlayer.play(
			$scope.settings.sound,
			$scope.settings.volume / 10);
		}
		$scope.$apply();
	    }

	    u = u.match(/\/([^\/]+)/);
	    if (u && (u = u[1])) {
		function update (d) {
		    $scope.allSettings = d;
		}

		$scope.settings = Settings.get({ username: u }, update);
		socket = io(url + 'stream/' + u);
		socket.on('message', onAlert);
		socket.on('update', update);
	    }

	    $scope.alertDone = function () {
		$scope.alert = false;
		$scope.$apply();
	    };
	}
    ]);
})(window.angular);
